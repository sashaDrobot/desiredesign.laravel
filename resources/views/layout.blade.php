<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Desire Design</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <style>
        .btn-more {
            color: #131313 !important;
            padding: 14px 17px;
            text-transform: uppercase;
            cursor: pointer;
            font-size: 1.2rem;
            border: none;
            border-radius: 5px;
            background: #e5c46d;
            background: linear-gradient(45deg,#e5c46d,#fdf19d 48%,#e5c46d 91%);
        }
    </style>
</head>
<body>
<div id="preloader" class="align-items-center justify-content-center" style="z-index: 10000">
    <svg width="77px" height="77px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
         preserveAspectRatio="xMidYMid" class="lds-square">
        <g transform="translate(20 20)">
            <rect x="-15" y="-15" width="30" height="30" fill="#e5c46d" transform="scale(0.868024 0.868024)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.4s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(50 20)">
            <rect x="-15" y="-15" width="30" height="30" fill="#fdf19d" transform="scale(0.58394 0.58394)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.3s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(80 20)">
            <rect x="-15" y="-15" width="30" height="30" fill="#fdf19d" transform="scale(0.332594 0.332594)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.2s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(20 50)">
            <rect x="-15" y="-15" width="30" height="30" fill="#fdf19d" transform="scale(0.58394 0.58394)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.3s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(50 50)">
            <rect x="-15" y="-15" width="30" height="30" fill="#e5c46d" transform="scale(0.332594 0.332594)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.2s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(80 50)">
            <rect x="-15" y="-15" width="30" height="30" fill="#e5c46d" transform="scale(0.255226 0.255226)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.1s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(20 80)">
            <rect x="-15" y="-15" width="30" height="30" fill="#fdf19d" transform="scale(0.332594 0.332594)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.2s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(50 80)">
            <rect x="-15" y="-15" width="30" height="30" fill="#e5c46d" transform="scale(0.255226 0.255226)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.1s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(80 80)">
            <rect x="-15" y="-15" width="30" height="30" fill="#fdf19d" transform="scale(0.496586 0.496586)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="0s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
    </svg>
</div>
<header class="container-fluid fixed-top">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-3 d-flex align-items-center justify-content-center logo-wrapper align-self-start">
                <a href="#">
                    <img src="img/logo.png" alt="DesireDesign" id="logo">
                </a>
            </div>
            <div class="col-6 d-flex align-items-center justify-content-center header-places">
                <p class="text-center mb-0">Dubai-Kiev-Odessa-London-Moscow</p>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center text-right">
                <div class="row header-contact-wrapper">
                    <div class="col-12 text-right lang-wrapper">
                        <a href="/setlocale/en" class="@if(app()->getLocale() == 'en') active-language @endif">EN</a>
                        |
                        <a href="/setlocale/ru" class="@if(app()->getLocale() == 'ru') active-language @endif">RU</a>
                    </div>
                    <div class="col-12 d-none d-md-block">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <a href="tel:+380501749682" class="pr-0">+380501749682</a><span></span>
                        <div class="w-100"></div>
                        <a href="tel:14037759922" class="pr-0">+14037759922</a>
                    </div>
                    <div class="col-12 d-block d-md-none">
                        <a href="tel:+380501749682">+380501749682</a><span><i class="fab fa-viber"></i> <i
                                    class="fab fa-telegram"></i> <i class="fab fa-whatsapp"></i></span>
                    </div>
                    <div class="col-12 d-block d-md-none">
                        <a href="tel:14037759922">+14037759922</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('content')

<script src="{{ asset('js/app.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/show-portfolio.js') }}"></script>
<script>
    new WOW().init();
</script>
<script>
    $('#multiple-items').slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        adaptiveHeight: true,
        arrows: false,
    });
    $('#next-slide-wrapper').click(() => {
        $('#multiple-items').slick('slickNext');
    });
    $('#prev-slide-wrapper').click(() => {
        $('#multiple-items').slick('slickPrev');
    });
</script>
<script>
    $("#more-btn").click(function (e) {
        let href = $(this).attr("href")
            , offsetTop = href === "#" ? 0 : $(href).offset().top - 120;
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 1500);
        e.preventDefault();
    });
</script>
<script>
    window.onload = () => {
        const device = navigator.userAgent.toLowerCase();
        let mob = device.match(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/);
        if (mob) {
            document.querySelector('.main-container>.row').style.backgroundAttachment = "scroll";
            document.querySelector('.room-images').classList.add('mobile');
            let darkBgs = document.querySelectorAll('.dark-bg');
            darkBgs.forEach((elem) => {
                elem.style.backgroundAttachment = "scroll";
            });
        }
    };
</script>
<script>
    $(window).on('load', function () {
        $preloader = $('#preloader'),
            $loader = $preloader.find('svg');
        setTimeout(function() {
            $loader.fadeOut();
            $preloader.delay(350).fadeOut('slow');
        }, 2000);
    });
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter51187760 = new Ya.Metrika2({
                    id:51187760,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/51187760" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>

@component('mail::message')
    # Новое сообщение

    Вопрос от клиента!

    Контактные данные:

    Имя: {{ $order->name }}
    @if($order->country)Страна/город: {{ $order->country }}@endif

    Контактный телефон: {{ $order->phone }} @if($order->messengers)(@foreach($order->messengers as $messenger) {{ $messenger }} @endforeach)@endif

    @if($order->email)Email: {{ $order->email }}@endif

    Вопрос: {{ $order->text }}

@endcomponent
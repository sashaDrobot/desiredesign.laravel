@extends('layout')

@section('content')
    <div class="container-fluid main-container">
        <div class="row">

        </div>
    </div>
    <div class="container-fluid interior-design">
        <div class="row py-5 light-bg">
            <div class="col-12 text-center wow slideInUp">
                <h1>{{ __('landing.company') }}</h1>
                <h3>
                    {{ __('landing.description') }}
                </h3>
            </div>
        </div>
    </div>
    <div class="container-fluid services-container dark-bg">
        <div class="row py-3">
            <div class="col-12 text-center container-header py-3">
                <h2 class="light-color wow slideInUp">{{ __('landing.services') }}</h2>
            </div>
            <div class="col-12 text-center services-wrapper wow slideInUp">
                <ul class="services light-color">
                    <li>{{ __('landing.design') }}</li>
                    <li>{{ __('landing.marketing') }}</li>
                    <li>{{ __('landing.psychological') }}</li>
                    <li>{{ __('landing.building') }}</li>
                    <li>{{ __('landing.unique') }}</li>
                    <li>{{ __('landing.smart') }}</li>
                    <li>{{ __('landing.home') }}</li>
                    <li>{{ __('landing.painting') }}</li>
                    <li>{{ __('landing.lighting') }}</li>
                </ul>
            </div>
        </div>
    </div>
    <div id="multiple-items-wrapper">
        <div id="prev-slide-wrapper" class="slide-control d-flex align-items-center justify-content-center">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </div>
        <div id="multiple-items">
            <div class="slide-wrapper d-flex justify-content-center align-items-center">
                <img src="img/slide_1.png" alt="">
            </div>
            <div class="slide-wrapper d-flex justify-content-center align-items-center">
                <img src="img/slide_2.jpeg" alt="">
            </div>
            <div class="slide-wrapper d-flex justify-content-center align-items-center">
                <img src="img/slide_4.jpg" alt="">
            </div>
            <div class="slide-wrapper d-flex justify-content-center align-items-center">
                <img src="img/slide_3.png" alt="">
            </div>
            <div class="slide-wrapper d-flex justify-content-center align-items-center">
                <img src="img/slide_5.png" alt="">
            </div>
            <div class="slide-wrapper d-flex justify-content-center align-items-center">
                <img src="img/slide_6.png" alt="">
            </div>
            <div class="slide-wrapper d-flex justify-content-center align-items-center">
                <img src="img/slide_7.png" alt="">
            </div>
            <div class="slide-wrapper d-flex justify-content-center align-items-center">
                <img src="img/slide_8.jpg" alt="">
            </div>
        </div>
        <div id="next-slide-wrapper" class="slide-control d-flex align-items-center justify-content-center">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </div>
    </div>
    <div class="container-fluid why-us-container light-bg">
        <div class="row py-3 wow slideInUp">
            <div class="col-12 text-center container-header py-3">
                <h2 class="dark-color">{{ __('landing.whyus') }}</h2>
            </div>
            <div class="col-12 text-center services-wrapper">
                <ul class="dark-color">
                    <li>{{ __('landing.key') }}</li>
                    <li>{{ __('landing.technologies') }}</li>
                    <li>{{ __('landing.manufacturers') }}</li>
                    <li>{{ __('landing.pictures') }}</li>
                </ul>
            </div>
        </div>
    </div>
    <div id="port-show-image" class="align-items-center justify-content-center">
        <div id="inner-image" class="d-flex align-items-center justify-content-center">
            <div id="image-wrapper" class="d-flex align-items-center justify-content-center">
                <img src="img/item_3.jpg" alt="">
                <div id="close-image"><i class="fa fa-times" aria-hidden="true"></i></div>
            </div>

        </div>
    </div>
    <div class="container-fluid portfolio-container dark-bg light-color">
        <div class="row pt-3">
            <div class="col-12 text-center container-header py-3">
                <h2 class="light-color">{{ __('landing.projects') }}</h2>
            </div>
        </div>
        <div class="row portfolio-elements-row py-3 wow slideInUp">
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_1.jpg" alt="" class="portfolio-elem-img img-fluid">
                </div>
            </div>

            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_6.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_2.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
        </div>
        <div class="row portfolio-elements-row py-3 wow slideInUp">
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_4.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_5.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_3.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
        </div>
        <div class="row portfolio-elements-row py-3 wow slideInUp">
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_7.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_8.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_9.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
              {{--  <h5 class="portfolio-elem-header my-1">{{ __('landing.commercial') }}</h5>--}}
            </div>
        </div>
        <div class="row portfolio-elements-row py-3 wow slideInUp">
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_10.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_11.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
            <div class="col-lg-4 portfolio-elem-wrapper text-center">
                <div class="portfolio-img-wrapper d-flex align-items-center justify-content-center my-1">
                    <img src="img/big_item_12.jpg" alt="" class="portfolio-elem-img img-fluid my-1">
                </div>
            </div>
        </div>
        <div class="row pb-3">
            <div class="w-100 d-flex justify-content-center">
                <a class="btn btn-more" href="https://drive.google.com/drive/folders/1aXY98--a0O4lF7x15B_e1I4Ntmo5avr3" target="_blank">Подробнее</a>
            </div>
        </div>
    </div>
    <div class="container-fluid offer-container light-bg dark-color">
        <div class="row">
            <div class="col-12 text-center container-header py-3">
                <h2 class="dark-color wow slideInUp">{{ __('landing.future') }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <a href="#form" id="more-btn">
                    <div class="down-btn d-flex align-items-center justify-content-center">
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div>
                </a>
            </div>
        </div>
        <div class="row room-images">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-12 text-right"><img src="img/room_1.png" alt="" class="img-fluid wow slideInUp">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-12 text-left"><img src="img/room_2.png" alt="" class="img-fluid wow slideInUp">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-left"><img src="img/room_3.png" alt="" class="img-fluid wow slideInUp">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid dark-bg light-color about-container">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center container-header pt-3 wow slideInUp">
                    <h2>{{ __('landing.about') }}</h2>
                    <h4 class="pt-3">{{ __('landing.virtual') }}</h4>
                </div>
            </div>
            <div class="row video-row">
                <div class="col-12 my-5 wow slideInUp">
                    <video preload="auto" controls="true" loop="loop" autoplay="autoplay" playsinline width="100%"
                           height="500px" muted poster="video/desire_design.png">
                        <source src="video/desire_design.webm" type='video/webm; codecs="vp8, vorbis"'>
                        <source src="video/desire_design.ogg" type='video/ogg; codecs="theora, vorbis"'>
                        <source src="video/desire_design.mp4" type='video/mp4'>

                        Тег video не поддерживается вашим браузером.
                        <a href="video/desire_design.mp4">Скачайте видео</a>.
                    </video>
                </div>
            </div>
        </div>
        <div class="row wow slideInUp">
            <div class="col-12 text-center">
                <p>
                    {{ __('landing.summary') }}
                </p>
            </div>
        </div>
    </div>
    <div class="container-fluid numbers-container light-bg dark-color">
        <div class="container py-3">
            <div class="row">
                <div class="col-12 text-center container-header wow slideInUp">
                    <h2>{{ __('landing.contacts') }}</h2>
                </div>
            </div>
            <div class="row numbers-row">
                <div class="col-2 d-none d-lg-block text-center py-3 wow slideInUp"><img src="img/golden_logo.png"
                                                                                         alt="logo" class="img-fluid">
                </div>
                <div class="col-lg-10 d-flex  justify-content-center flex-column wow slideInUp">
                    <div class="row telephone-numbers-wrapper py-2">
                        <div class="col-lg-4 offset-lg-1"><a href="tel:14037759922"><strong>Dubai</strong> +14037759922</a>
                        </div>
                        <div class="col-lg-4 text-right"><a href="tel:447482878318"><strong>London</strong> 447482878318</a>
                        </div>
                    </div>
                    <div class="row telephone-numbers-wrapper py-2">
                        <div class="col-lg-4 offset-lg-1"><a href="tel:+380501749682"><strong>Ukraine</strong>
                                +380501749682 <i class="fab fa-viber"></i> <i class="fab fa-telegram"></i> <i
                                        class="fab fa-whatsapp"></i> </a></div>
                        <div class="col-lg-4 text-right"><a href="tel:+79588149888"><strong>Moscow</strong> +79588149888</a>
                        </div>
                    </div>
                    <div class="row telephone-numbers-wrapper py-2">
                        <div class="col-lg-4 offset-lg-1"><a href="tel:14037759922"><strong>International</strong>
                                14037759922</a></div>
                        <div class="col-lg-4 text-right"><a
                                    href="mailto:DesireDesignDD@gmail.com">DesireDesignDD@gmail.com</a></div>
                    </div>
                    <div class="row telephone-numbers-wrapper py-2">
                        <div class="col-lg-4 offset-lg-1"><a href="https://www.instagram.com/desire.design.company/"><i
                                        class="fab fa-instagram mr-1"></i>desire.design.company</a></div>
                        <div class="col-lg-4 text-right"><a href="https://www.facebook.com/design.virtual.net"><i
                                        class="fab fa-facebook mr-1"></i>design.virtual.net</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid contacts-container dark-bg light-color pb-5" id="form">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 form-wrapper py-4 my-5 wow slideInUp">
                    <div class="row contacts-form mb-3">
                        <div class="col-12 text-center mb-3">
                            <h4>{{ __('landing.order') }}</h4>
                        </div>
                        <form action="/" method="post" id="feedback" class="col-12">
                            @csrf
                            <div class="row py-2">
                                <div class="col-sm-4 offset-sm-1">
                                    <label for="name">{{ __('landing.name') }}</label>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <input type="text" id="name" name="name" required>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-sm-4 offset-sm-1">
                                    <label for="phone">{{ __('landing.contact') }}</label>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <input type="tel" id="phone" name="phone" required>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-sm-4 offset-sm-1">
                                    <label for="email">{{ __('landing.email') }}</label>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <input type="email" id="email" name="email">
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-sm-4 offset-sm-1">
                                    <label for="text">{{ __('landing.question') }}</label>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <textarea name="text" id="text" cols="30" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-12 text-center">
                                    <button id="load" type="submit">{{ __('landing.send-order') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="pop-up-form-wrapper">
        <input type="checkbox" name="pop-up-btn" class="d-none" id="pop-up-btn">
        <label for="pop-up-btn" class="d-flex align-items-center justify-content-center" id="pop-up-btn-label">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <i class="fa fa-times" aria-hidden="true"></i>
        </label>
        <div id="pop-up-form">
            <div class="row">
                <div class="col-12 form-wrapper">
                    <div class="row contacts-form">
                        <form action="/" method="post" id="pop-up-submit" class="col-12 text-center">
                            @csrf
                            <div class="row py-2">
                                <div class="col-12">
                                    <label for="name_1">{{ __('landing.name') }}</label>
                                </div>
                                <div class="col-12 text-center">
                                    <input type="text" id="name_1" name="name" required>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-12">
                                    <label for="country">{{ __('landing.country') }}</label>
                                </div>
                                <div class="col-12 text-center">
                                    <input type="text" id="country" name="country" maxlength="40">
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-12">
                                    <label for="phone_1" class="mb-0">{{ __('landing.contact') }}</label>
                                </div>
                                <div class="col-12 text-center messenger-choice">
                                    <input type="checkbox" name="viber" id="viber" class="d-none">
                                    <input type="checkbox" name="telegram" id="telegram" class="d-none">
                                    <input type="checkbox" name="whatsapp" id="whatsapp" class="d-none">
                                    <label for="viber" id="viber-label">
                                        <i class="fab fa-viber"></i>
                                    </label>
                                    <label for="telegram" id="telegram-label">
                                        <i class="fab fa-telegram"></i>
                                    </label>
                                    <label for="whatsapp" id="whatsapp-label">
                                        <i class="fab fa-whatsapp"></i>
                                    </label>
                                </div>
                                <div class="col-12 text-center">
                                    <input type="tel" id="phone_1" name="phone" required>
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-12">
                                    <label for="text_1">{{ __('landing.question') }}</label>
                                </div>
                                <div class="col-12 text-center">
                                    <textarea name="text" id="text_1" cols="30" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="row pt-0">
                                <div class="col-12 text-center">
                                    <button id="pop-up-load" type="submit">{{ __('landing.send') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
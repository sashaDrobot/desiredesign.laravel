<?php

return [

    'company' => 'Международная Дизайнерская Компания',
    'description' => 'Мы воплощаем пространство вашей мечты в реальности в любой точке мира',

    'services' => 'Наши услуги',
    'design' => '-Архитектура и Дизайн в Виртуальной VR Реальности',
    'marketing' => '-Виртуальные шоурумы и офисы продаж',
    'psychological' => '-Дизайн интерьера под ключ',
    'building' => '-Архитектура и Инженерия зданий',
    'unique' => '-Продающие коммерческие интерьеры',
    'smart' => '-Параметрическая архитектура, BIM технологии',
    'home' => '-Умный дом и профессиональная акустика',
    'painting' => '-Интерьерная живопись',
    'lighting' => '-Дизайн экстерьерного и интерьерного освещения',

    'whyus' => 'Почему именно мы?',
    'key' => '-Закрытие объектов под ключ',
    'technologies' => '-Использование новейших технологий VR',
    'manufacturers' => '-Многолетняя работа с Ведущими производителями со всего Мира',
    'pictures' => '-Мы не только рисуем красивые Картинки, мы их и воплощаем в Реальность',

    'projects' => 'Наши проекты',
    'classic' => 'Стиль: Классика',
    'minimalism' => 'Стиль: Лофт и минимализм',
    'loft-modern' => 'Стиль: Лофт (современный стиль)',
    'modern' => 'Стиль: Модерн',
    'art-deco' => 'Стиль Арт-Деко',
    'commercial' => 'Коммерческие проекты',

    'future' => 'Закажите проект будущего интерьера прямо сейчас!',

    'about' => 'О НАШЕЙ КОМПАНИИ',
    'virtual' => 'Виртуальный дизайн. Ощути дизайн до его воплощения.',

    'information' => 'Информация о нашей компании',

    'summary' => 'Наша компания на рынке архитектуры и дизайна уже 7 лет и имеет два главных направления: коммерческая и жилая недвижимость. 
                    Мы воплощаем в жизнь все Ваши идеи стильного и яркого интерьера, в котором присутствуют все модные тенденции, чувство вкуса и комфорта! 
                    Наша творческая профессиональная команда, делает каждый проект уникальным и неповторимым, начиная от создания архитектуры и дизайна и заканчивая комплектацией! 
                    Мы влюбляемся в каждый наш проект, и делаем все просто идеально! 
                    Воплощайте свои мечты с нами!',

    'contacts' => 'Контакты',

    'order' => 'ЗАПОЛНИТЕ ЗАЯВКУ ПРЯМО СЕЙЧАС, И МЫ СВЯЖЕМСЯ С ВАМИ',
    'name' => 'Ваше имя',
    'contact' => 'Контактный телефон',
    'email' => 'E-mail',
    'question' => 'Ваш вопрос',

    'send-order' => 'Отправить заявку',
    'send' => 'Отправить',

    'success' => 'Успешно отправлено!',

    'country' => 'Страна/город',

];
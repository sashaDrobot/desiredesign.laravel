<?php

return [

    'company' => 'International Design Company',
    'description' => 'We implement your dream design to reality in any part of the world',

    'services' => 'Our services',
    'design' => '-Architecture and design in Virtual Reality (VR)',
    'marketing' => '-Virtual showrooms and property sales offices',
    'psychological' => '-Interior design turn-key',
    'building' => '-Architecture with full engineering project',
    'unique' => '-Commercial interiors',
    'smart' => '-Parametric architecture, BIM architecture',
    'home' => '-Smart house and professional acoustics',
    'painting' => '-Interior paintings',
    'lighting' => '-Exterior and Interior lighting design',

    'whyus' => 'Why us?',
    'key' => '-Turnkey construction',
    'technologies' => '-Using the innovative VR technology',
    'manufacturers' => '-Years of cooperation with leading furniture manufacturers all over the world',
    'pictures' => '-We ate not only making beautiful pictures, we implement our design into reality',

    'projects' => 'Our projects',
    'classic' => 'Style: Classic',
    'minimalism' => 'Style: Loft and minimalism',
    'loft-modern' => 'Style: Loft (modern style)',
    'modern' => 'Style: Modern',
    'art-deco' => 'Style: Art-Deco',
    'commercial' => 'Commercial projects',

    'future' => 'Order your future interior project right NOW!',

    'about' => 'About us',
    'virtual' => 'Virtual reality design. Feel the design before its realization.',

    'information' => 'Information about our company',

    'summary' => 'Our company has been sussessfully working in architecture and design market for 7 years, and has two main directions: commercial and residential real estate. 
                    We bring to life all your ideas of a stylish, trendy and comfortable design, which suits your personality and underlines the elegance.
                    Our creative professional team makes each project unique and inimitable, starting from the architecture and design and ending with a furniture!
                    We implement your design dreams!',

    'contacts' => 'Contacts',

    'order' => 'FILL IN THE APPLICATION NOW, AND WE WILL CONTACT YOU',
    'name' => 'Your name',
    'contact' => 'Contact phone',
    'email' => 'E-mail',
    'question' => 'Your question',

    'send-order' => 'Send',
    'send' => 'Send',

    'success' => 'Successfully  sent!',

    'country' => 'Country/city',

];
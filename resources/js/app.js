require('./bootstrap');

window.Vue = require('vue');

$(document).ready(function () {
    let locale = $('html').attr('lang');
    let err = locale === 'ru' ? 'Ошибка!' : 'Error!';
    let proces = locale === 'ru' ? 'обработка' : 'processing';
    let send_order = locale === 'ru' ? 'Отправить заявку' : 'Send';
    let send = locale === 'ru' ? 'Отправить' : 'Send';

    $('#feedback').submit(function (event) {
        event.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: new FormData(this),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#load').html(`<i class="fa fa-circle-o-notch fa-spin"></i> ${proces}`).prop('disabled', true);
            },
            success: function (result) {
                $('#load').html(result.msg).prop('disabled', true);
            },
            error: function () {
                $('#load').html(`${err}...`).prop('disabled', true);
            },
            complete: function () {
                setTimeout(() => {
                    $('#load').html(`${send_order}`).prop('disabled', false);
                }, 3000);
                $('#feedback input, #feedback textarea').val("");
            }
        });
    });

    $('#pop-up-submit').submit(function (event) {
        event.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: new FormData(this),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#pop-up-load').html(`<i class="fa fa-circle-o-notch fa-spin"></i> ${proces}`).prop('disabled', true);
            },
            success: function (result) {
                $('#pop-up-load').html(result.msg).prop('disabled', true);
            },
            error: function () {
                $('#pop-up-load').html(`${err}...`).prop('disabled', true);
            },
            complete: function () {
                setTimeout(() => {
                    $('#pop-up-load').html(`${send}`).prop('disabled', false);
                }, 3000);
                $('#pop-up-submit input, #pop-up-submit textarea').val("");
            }
        });
    });
});
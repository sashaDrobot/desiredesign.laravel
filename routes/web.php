<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::post('/', 'MainController@feedback');

Route::get('/setlocale/{locale}', 'LocalizationController@setLocale');

Route::get('/mailable', function () {
    $order = App\Order::find(1);
    return new App\Mail\SendOrder($order);
});
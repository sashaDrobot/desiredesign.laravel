<?php

namespace App\Http\Controllers;

use App\Mail\SendOrder;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function feedback(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:200',
            'phone' => 'required|max:20',
            'text' => 'required|max:500',
            'email' => 'sometimes|nullable|string|email|max:100',
            'country' => 'sometimes|nullable|string|max:100',
            'viber' => 'sometimes|nullable',
            'telegram' => 'sometimes|nullable',
            'whatsapp' => 'sometimes|nullable',
        ]);

        $order = new Order;

        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->text = $request->text;

        $messengers = [];

        if ($request->filled('email')) {
            $order->email = $request->email;
        }
        if ($request->filled('country')) {
            $order->country = $request->country;
        }
        if ($request->filled('viber')) {
            $messengers[] = 'viber';
        }
        if ($request->filled('telegram')) {
            $messengers[] = 'telegram';
        }
        if ($request->filled('whatsapp')) {
            $messengers[] = 'whatsapp';
        }

        $order->messengers = $messengers;

        $order->save();

        Mail::to('desiredesigndd@gmail.com')
            ->send(new SendOrder($order));

        $response = Lang::get('landing.success');

        return response()->json(array('msg' => $response), 200);
    }
}
